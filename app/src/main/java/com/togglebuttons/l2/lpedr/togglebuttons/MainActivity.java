package com.togglebuttons.l2.lpedr.togglebuttons;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

/**Tip Calculator
 * @author Lorenzo Pedroza (based on tutorial by Paulo Dichone)
 * */

public class MainActivity extends AppCompatActivity {
    private ToggleButton toggleButton;
    private TextView resultView;
    private CheckBox spookedCheckBox;
    private CheckBox notspookedCheckBox;
    private Button spookRatingButton;
    private TextView resultSurveyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultView = findViewById(R.id.peekABooID);

        toggleButton = findViewById(R.id.toggleButtonID);

        spookedCheckBox = findViewById(R.id.spookedCheckBoxID);
        notspookedCheckBox = findViewById(R.id.notSpookedTextBoxID);
        spookRatingButton = findViewById(R.id.submitSpookButtonID);
        resultSurveyText = findViewById(R.id.resultSurveyTextID);

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    resultView.setVisibility(View.VISIBLE);
                    spookedCheckBox.setVisibility(View.VISIBLE);
                    notspookedCheckBox.setVisibility(View.VISIBLE);
                    spookRatingButton.setVisibility(View.VISIBLE);
                    spookRatingButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append(spookedCheckBox.getText().toString()+" status is: " +
                            spookedCheckBox.isChecked() + "\n");
                            stringBuilder.append(notspookedCheckBox.getText().toString()+ "status is: " +
                            notspookedCheckBox.isChecked()+"\n");
                            resultSurveyText.setText(stringBuilder); //TODO make invisible as needed
                        }
                    });
                }
                else{
                    resultView.setVisibility(View.INVISIBLE);
                    spookedCheckBox.setVisibility(View.INVISIBLE);
                    notspookedCheckBox.setVisibility(View.INVISIBLE);
                    spookRatingButton.setVisibility(View.INVISIBLE);
                }
            }
        });
    }
}
